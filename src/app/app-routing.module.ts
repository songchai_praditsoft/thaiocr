import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MemberComponent } from './member/member.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ScanDocumentsComponent } from './scan-documents/scan-documents.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { ProfileComponent } from './profile/profile.component';
import { PackageComponent } from './package/package.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
const routes: Routes = [
  { path: '',   redirectTo: '/welcome', pathMatch: 'full' },
  {
    path: 'welcome', component : WelcomeComponent
  },
  {
    path : 'member',component :MemberComponent
  },
  {
    path : 'scan',component :ScanDocumentsComponent
  },
  {
    path : 'main-nav',component :MainNavComponent
  },
  {
    path : 'profile',component :ProfileComponent
  },
  {
    path : 'package',component :PackageComponent
  },
  {
    path : 'profile-edit',component : ProfileEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
