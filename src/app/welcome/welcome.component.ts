import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  statusLogin:string = ""
  loginForm(dataForm:NgForm){
    console.log(dataForm.value)
  }

  signupData:any = {
    'email' : '',
    'password' : '',
    'passwordConfirm' : ''
  }

  signupForm(formLogin:NgForm){
    this.signupData.email = formLogin.value.email
    this.signupData.password = formLogin.value.password
    this.signupData.passwordConfirm = formLogin.value.passwordConfirm

    console.log(this.signupData)
  }

}
