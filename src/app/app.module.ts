import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { MemberComponent } from './member/member.component';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ScanDocumentsComponent } from './scan-documents/scan-documents.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatTooltipModule} from '@angular/material/tooltip';
import { Profile } from 'selenium-webdriver/firefox';
import { ProfileComponent } from './profile/profile.component';
import { PackageComponent } from './package/package.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';



@NgModule({
  declarations: [
    AppComponent,
    PackageComponent,
    ProfileComponent,
    WelcomeComponent,
    MemberComponent,
    ScanDocumentsComponent,
    MainNavComponent,
    ProfileEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
