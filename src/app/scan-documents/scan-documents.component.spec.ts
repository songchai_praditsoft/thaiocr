import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanDocumentsComponent } from './scan-documents.component';

describe('ScanDocumentsComponent', () => {
  let component: ScanDocumentsComponent;
  let fixture: ComponentFixture<ScanDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
